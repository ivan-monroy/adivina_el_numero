use std::io;
use rand::Rng;
use std::cmp::Ordering;

fn main() {
    println!("Adivina el numero!!");
    
    // Genera el numero secreto entre 1 y 101
    let num_secreto = rand::thread_rng().gen_range(1, 101);

    loop {
        // Lee el numero introducido por el user 
        println!("Por favor introduce un número:");
        let mut guess = String::new();
        io::stdin().read_line(&mut guess).expect("Error al leer el input");
        // Parsea el string a int (si no es numero ignora)
        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("El numero introducido es {}", guess);

        match guess.cmp(&num_secreto) {
            Ordering::Less => println!("Demasiado pequeño!"),
            Ordering::Greater => println!("Demasiado grande!"),
            Ordering::Equal => {
                println!("Acertaste!");
                break;
            }
        }
    }
}
